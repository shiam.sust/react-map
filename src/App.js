import React from 'react';
import './App.css';

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

const MapWithAMarker = withScriptjs(withGoogleMap(props =>
  <GoogleMap
    defaultZoom={10}
    defaultCenter={{ lat: -34.397, lng: 150.644 }}
  >
    <Marker
      position={{ lat: props.lat, lng: props.lon }}
    />
  </GoogleMap>
));

function App() {
  return (
    <>
      <MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9rkyVN3BBoVNjpp_R5nLJIA6S1z9Ah_E&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `1000px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        lat = "24.847560"
        lon = "89.373713"
      />
    </>
  );
}

export default App;
